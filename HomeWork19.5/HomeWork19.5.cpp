#include <iostream>
#include <string>
#include <string>



class animal
{
private:
	std::string x = "xxx";

public:
	animal() {}

	virtual void voice()
	{
		std::cout << x << "\n";
	}


};
class Dog : public animal
{
private:
	std::string x = "Woof";
public:
	Dog() {}
	void voice() override
	{
		std::cout << x << '\n';
	}

};

class Cat : public animal
{
private:
	std::string x = "Meu";
public:
	Cat() {}
	void voice() override
	{
		std::cout << x << '\n';
	}

};

class Cow : public animal
{
private:
	std::string x = "moo";
public:
	Cow() {}
	void voice() override
	{
		std::cout << x << '\n';
	}
};



int main()
{
	Dog* a = new Dog;
	Cat* b = new Cat;
	Cow* c = new Cow;
	animal** array = new animal * [3]{ a,b,c };
	for (int i = 0; i <= 2; i++)
	{
		array[i]->voice();
	}

}